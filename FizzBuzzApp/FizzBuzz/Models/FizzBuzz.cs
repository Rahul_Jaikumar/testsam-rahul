﻿namespace FizzBuzz.Models
{
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    public class FizzBuzz
    {
        [Display(Name = "Enter a number")]
        [Required(ErrorMessage = "Enter a valid number")]
        [Range(1, 1000, ErrorMessage = "Enter the number between 1 and 1000")]
        public int InputNumber { get; set; }

        public IPagedList<string> DisplayList { get; set; }
        public int PageNumber { get; set; }
    }
}