﻿namespace FizzBuzz.Business.Interfaces
{
    using System;

    public interface IDayCheckerRule
    {
        DayOfWeek SpecifiedDayOfWeek { get; set; }

        bool IsSpecifiedDayOfWeek(DateTime date);
    }
}